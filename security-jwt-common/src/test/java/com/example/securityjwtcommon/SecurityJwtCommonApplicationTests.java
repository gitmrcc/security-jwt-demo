package com.example.securityjwtcommon;

import com.example.securityjwtcommon.utils.RsaUtils;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SecurityJwtCommonApplicationTests {

    @Test
    void test() {
        System.out.println("3");
    }

    @Test
    void contextLoads() throws Exception {
        String privateKey = "D:/all_data/document/project/springdemo/security-jwt-demo/rsa/auth_key/id_key_rsa";
        String publicKey = "D:/all_data/document/project/springdemo/security-jwt-demo/rsa/auth_key/id_key_rsa.pub";
        RsaUtils.generateKey(publicKey, privateKey, "security-jwt", 1024);
    }

}
