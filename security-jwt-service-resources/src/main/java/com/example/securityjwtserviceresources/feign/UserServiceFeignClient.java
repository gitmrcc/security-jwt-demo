package com.example.securityjwtserviceresources.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient("authc-service")
public interface UserServiceFeignClient {

    @RequestMapping(value = "/user/service", method = RequestMethod.POST, headers = {"User-Agent", "Chrome/69.0.3497.81 Safari/537.36"})
    public Map getService();

}
