package com.example.securityjwtserviceresources.interceptor;

import com.example.securityjwtserviceresources.domain.Const;
import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * Feign请求拦截器
 */
public class FeignBasicAuthRequestInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        System.out.println("feign拦截器执行");
        template.header("Authorization", "Bearer "+ Const.threadLocal.get());
    }
}
