package com.example.securityjwtserviceresources.controller;

import com.example.securityjwtserviceresources.feign.UserServiceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceFeignClient userServiceFeignClient;

    @RequestMapping("/query")
    public Map query(){
        System.out.println("方法执行前");
        return userServiceFeignClient.getService();
    }

    @RequestMapping("/update")
    public String update(){
        return "update";
    }

}
