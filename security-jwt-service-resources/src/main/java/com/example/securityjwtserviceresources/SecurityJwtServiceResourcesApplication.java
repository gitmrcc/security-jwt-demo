package com.example.securityjwtserviceresources;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@MapperScan("com.example.securityjwtserviceresources.dao")
public class SecurityJwtServiceResourcesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityJwtServiceResourcesApplication.class, args);
    }

}
