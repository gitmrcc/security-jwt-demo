package com.example.securityjwtauthcserver.controller;

import com.example.securityjwtauthcserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @PostMapping("/service")
    public Map getService(){
        Map resultMap = new HashMap();
        resultMap.put("code", HttpServletResponse.SC_OK);
        resultMap.put("msg", "authc-server的方法");
        return resultMap;
    }

}
