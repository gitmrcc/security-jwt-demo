package com.example.securityjwtauthcserver;

import com.example.securityjwtauthcserver.config.RsaKeyProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@MapperScan("com.example.securityjwtauthcserver.dao")
@EnableConfigurationProperties(RsaKeyProperties.class)
public class SecurtiyJwtAuthcServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurtiyJwtAuthcServerApplication.class, args);
    }

}
