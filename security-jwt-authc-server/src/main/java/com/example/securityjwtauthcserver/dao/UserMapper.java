package com.example.securityjwtauthcserver.dao;

import com.example.securityjwtauthcserver.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

@Mapper
public interface UserMapper {
    public User queryByUserName(@Param("userName") String userName);
}
